import React from 'react';
import {Provider} from 'react-redux';
import {store} from './redux/store';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import ContactsPage from './pages/ContactsPage';
import ContactDetailPage from './pages/ContactDetailPage';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Provider store={store}>
        <Stack.Navigator initialRouteName="Contatcs">
          <Stack.Screen name="Contacts" component={ContactsPage} />
          <Stack.Screen name="Details" component={ContactDetailPage} />
        </Stack.Navigator>
      </Provider>
    </NavigationContainer>
  );
};
export default App;

import React, {useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import ListContacts from '../components/ListContacts';

const ContactsPage = ({navigation}) => {
  const handleNavigation = useCallback(
    (id, dataId) => {
      navigation.navigate('Details', {
        dataId,
        itemId: id,
      });
    },
    [navigation],
  );

  return (
    <View style={Styles.container}>
      <ListContacts onPress={handleNavigation} />
    </View>
  );
};
const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
export default ContactsPage;

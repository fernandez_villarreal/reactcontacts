import React from 'react';
import {StyleSheet, View, ScrollView, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import TitleContact from '../components/TitleContact';
import OptionContactButton from '../components/OptionContactButton';
import InfoContactCard from '../components/InfoContactCard';

const ContactDetailPage = ({route}) => {
  const contact = useSelector(state =>
    state.contacts
      .find(letterContact => letterContact.title === route.params.dataId)
      ?.data.find(item => item.id === route.params.itemId),
  );

  return (
    <View style={styles.container}>
      <ScrollView>
        <TitleContact
          firstName={contact.firstName}
          lastName={contact.lastName}
        />
        <View style={styles.optionsButtons}>
          <OptionContactButton title="message" />
          <OptionContactButton title="call" />
          <OptionContactButton title="video" />
          <OptionContactButton title="mail" />
        </View>

        {contact.phones?.length > 0 && (
          <View style={styles.infoContainer}>
            {contact.phones.map((phone, index) => (
              <InfoContactCard
                key={`phone-${index}`}
                title={phone.type}
                detail={phone.number}
              />
            ))}
          </View>
        )}

        {contact.emailWork && (
          <View style={styles.infoContainer}>
            <InfoContactCard title="work" detail={contact.emailWork} />
          </View>
        )}

        {contact.addresses?.length > 0 && (
          <View style={styles.infoContainer}>
            {contact.addresses.map((adress, index) => (
              <InfoContactCard
                key={`adress-${index}`}
                title={adress.type}
                detail={adress.street}
              />
            ))}
          </View>
        )}

        {contact.birthDate && (
          <View style={styles.infoContainer}>
            <InfoContactCard title="birthday" detail={contact.birthDate} />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  optionsButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    marginRight: 10,
    marginLeft: 7,
  },
  infoContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    margin: 10,
  },
});

export default ContactDetailPage;

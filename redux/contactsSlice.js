import {createSlice} from '@reduxjs/toolkit';

const dummyContacts = [
  {
    title: 'L',
    data: [
      {
        id: 1,
        firstName: 'Alan',
        lastName: 'Luciano',
        company: 'Alans Company',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'alansComapny@company.com',
        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
      {
        id: 4,
        firstName: 'Alan 4',
        lastName: 'Luciano 4',
        company: 'Alans Company',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'alansComapny4@company.com',
        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
    ],
  },
  {
    title: 'F',
    data: [
      {
        id: 2,
        firstName: 'Roberto',
        lastName: 'Fernandez',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'robertoCompany@company.com',
        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
      {
        id: 6,
        firstName: 'Roberto 6',
        lastName: 'Fernandez 6',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'robertoCompany6@company.com',

        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
    ],
  },
  {
    title: 'Z',
    data: [
      {
        id: 3,
        firstName: 'Sandy',
        lastName: 'Zuleta',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'sandyCompany@company.com',
        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
      {
        id: 7,
        firstName: 'Sandy 7',
        lastName: 'Zuleta 7',
        birthDate: '01/01/1901',
        addresses: [
          {type: 'home', street: '123 Main St'},
          {type: 'work', street: '456 Main St'},
        ],
        emailWork: 'sandyCompany7@company.com',
        phones: [
          {type: 'home', number: '123-456-7890'},
          {type: 'work', number: '123-456-7890'},
        ],
      },
    ],
  },
];

const contactsSlice = createSlice({
  name: 'contacts',
  initialState: dummyContacts,
  reducers: {
    addContact(state, action) {
      state.push(action.payload);
    },
  },
});

export const {addContact} = contactsSlice.actions;
export default contactsSlice.reducer;

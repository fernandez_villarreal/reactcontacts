import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const InfoContactCard = ({title, detail}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.containerText}>{title}</Text>
      <Text style={(styles.containerText, styles.detailText)}>{detail}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 10,
    height: 40,
  },
  containerText: {
    textAlign: 'left',
  },
  detailText: {
    color: '#0e23e6',
  },
});

export default InfoContactCard;

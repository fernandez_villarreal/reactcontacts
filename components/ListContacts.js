import React from 'react';
import {SectionList} from 'react-native';
import ItemContact from './ItemContact';
import ListContatsTitle from './ListContatsTitle';
import {useSelector} from 'react-redux';

const ListContacts = ({onPress}) => {
  const {contacts} = useSelector(state => state);

  return (
    <>
      <SectionList
        sections={contacts}
        keyExtractor={(item, index) => item + index}
        renderItem={({item, section}) => (
          <ItemContact
            firstName={item.firstName}
            lastName={item.lastName}
            id={item.id}
            sectionID={section.title}
            onPress={onPress}
          />
        )}
        renderSectionHeader={({section: {title}}) => (
          <ListContatsTitle title={title} />
        )}
      />
    </>
  );
};

export default ListContacts;

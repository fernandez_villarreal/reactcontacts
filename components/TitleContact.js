import React, {useMemo} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const TitleContact = ({firstName, lastName}) => {
  return (
    <View style={styles.container}>
      <Initials firstName={firstName} lastName={lastName} />
      <Text style={styles.fullName}>
        {firstName} {lastName}
      </Text>
    </View>
  );
};

const Initials = ({firstName, lastName}) => {
  const fullName = useMemo(
    () => `${firstName} ${lastName}`,
    [firstName, lastName],
  );
  return (
    <View style={styles.inicialContainer}>
      <Text
        adjustsFontSizeToFit
        numberOfLines={1}
        minimumFontScale={0.01}
        style={styles.placeholderText}>
        {getAvatarInitials(fullName)}
      </Text>
    </View>
  );
};

const getAvatarInitials = textString => {
  if (!textString) {
    return '';
  }
  const text = textString.trim();

  const textSplit = text.split(' ');

  if (textSplit.length <= 1) {
    return text.charAt(0);
  }
  const initials =
    textSplit[0].charAt(0) + textSplit[textSplit.length - 1].charAt(0);

  return initials;
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullName: {
    fontSize: 30,
    textAlign: 'center',
  },
  inicialContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#808080',
    width: 80,
    height: 80,
    borderRadius: Math.round(40 + 40) / 2,
    marginBottom: 10,
    marginTop: 10,
  },
  placeholderText: {
    textAlign: 'center',
    fontWeight: '700',
    color: '#ffffff',
    fontSize: Math.round(40) / 2,
    width: '100%',
  },
});
export default TitleContact;

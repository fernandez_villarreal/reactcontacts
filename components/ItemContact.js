import React, {useCallback} from 'react';
import {View, Text, StyleSheet, TouchableHighlight} from 'react-native';

const ItemContact = ({onPress, firstName, lastName, id, sectionID}) => {
  const handlePress = useCallback(() => {
    onPress(id, sectionID);
  }, [id, sectionID, onPress]);

  return (
    <TouchableHighlight onPress={handlePress}>
      <View style={styles.itemContainer}>
        <View style={styles.rightSectionContainer}>
          <View style={styles.rightTextContainer}>
            <Text>
              {firstName} {lastName}
            </Text>
          </View>
        </View>
      </View>
    </TouchableHighlight>
  );
};

export default ItemContact;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    minHeight: 44,
    height: 50,
  },
  rightSectionContainer: {
    marginLeft: 18,
    marginRight: 18,
    flexDirection: 'row',
    flex: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#999',
    marginBottom: 10,
  },
  rightTextContainer: {
    justifyContent: 'center',
    marginRight: 10,
  },
});

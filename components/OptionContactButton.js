import React from 'react';
import {Text, TouchableHighlight, StyleSheet} from 'react-native';

const OptionContactButton = ({title}) => {
  return (
    <TouchableHighlight style={styles.optionButton}>
      <Text style={styles.title}>{title}</Text>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  optionButton: {
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 10,
    width: 90,
    height: 50,
  },
  title: {
    fontSize: 9,
    textAlign: 'center',
  },
});

export default OptionContactButton;
